//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LVL2_ASPNet_MVC_08.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class db_batch7
    {
        public int Id { get; set; }
        public Nullable<double> NISN { get; set; }
        public string Nama { get; set; }
        public string Kelamin { get; set; }
        public Nullable<double> Tingkat { get; set; }
    }
}
